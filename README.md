## Como Rodar o Projeto:

* Após clonar o repositório, instale as dependências necessárias que o projeto necessita para funcionar corretamente, através do comando 'npm install' no seu terminal, no diretório do projeto.
* O próximo passo trata-se de iniciar o servidor e de fato, rodar o projeto. Para isso, também via terminal, execute 'npm start' no diretório do projeto.