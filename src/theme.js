const theme = {
  color_1: '#a5a5a5',
  color_2: '#fff',
  color_3: '#484848',
  color_4: '#39b54a',
  color_5: '#767676'
};

export default theme;