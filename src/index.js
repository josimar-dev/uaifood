import React from 'react';
import ReactDOM from 'react-dom';
import Main from './Main';
import { BrowserRouter, withRouter } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
import globalStyles from './globalStyles';
import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import { selectedCity, restaurantResults, cityId, foods, cost, rating } from './reducers/reducers';
import initialState from './initialState';

const allReducers = combineReducers({
  selectedCity,
  cityId,
  restaurantResults,
  foods,
  cost,
  rating
});

const store = createStore(
  allReducers,
  initialState,
  window.devToolsExtension && window.devToolsExtension()
);

globalStyles();

const App = withRouter(props => <Main {...props} />);

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </BrowserRouter>,
  document.getElementById('root')
);
registerServiceWorker();
