const urlAPI = 'https://developers.zomato.com/api/v2.1/';

export const getCitiesDetails = city => {
  let url = `${urlAPI}cities?q=${city}&count=1`;
  let params = {
    headers: {
      "user-key": "a467ca773098f6ccd0bef13c105a808f"
    },
    method: "GET"
  };
  return fetch(url, params)
    .then(response => response.json());
}

export const searchRestaurants = obj => {
  let url = `${urlAPI}search?entity_id=${obj.city_id}&entity_type=city&start=0&count=12&cuisines=${obj.cuisines}&sort=${obj.sort}`;
  let params = {
    headers: {
      "user-key": "a467ca773098f6ccd0bef13c105a808f"
    },
    method: "GET"
  };
  return fetch(url, params)
    .then(response => response.json());
}