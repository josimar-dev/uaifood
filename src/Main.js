import React, { Component, Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './views/Home';
import Restaurants from './views/Restaurants';
import Container from './components/Container'

class Main extends Component {
  render() {
    const { pathname } = this.props.location;
    const { history } = this.props;
    return (
      <Fragment>
        <Switch>
          <Route exact path='/' component={() => <Home history={history} />}/>
          <Route path='/restaurants' component={() => <Restaurants history={history} />}/>
        </Switch>
      </Fragment>
    );
  }
}

export default Main;