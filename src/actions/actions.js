export const UPDATE_SELECTED_CITY = 'selectedCity:updateSelectedCity';
export const CALL_RESTAURANTS_RESULTS = 'restaurantResults:callRestaurantsResults';
export const UPDATE_CITY_ID = 'cityId:updateCityId';
export const UPDATE_FOODS = 'foods:updateFoods';
export const UPDATE_COST = 'cost:updateCost';
export const UPDATE_RATING = 'rating:updateRating';

export function updateRating (rating) {
  return {
    type: UPDATE_RATING,
    payload: {
      rating: rating
    }
  }
}

export function updateCost (cost) {
  return {
    type: UPDATE_COST,
    payload: {
      cost: cost
    }
  }
}

export function updateFoods (foods) {
  return {
    type: UPDATE_FOODS,
    payload: {
      foods: foods
    }
  }
}

export function updateSelectedCity (city) {
  return {
    type: UPDATE_SELECTED_CITY,
    payload: {
      selectedCity: city
    }
  }
}

export function callRestaurantsResults (results) {
  return {
    type: CALL_RESTAURANTS_RESULTS,
    payload: {
      restaurantResults: results
    }
  }
}

export function updateCityId (id) {
  return {
    type: UPDATE_CITY_ID,
    payload: {
      cityId: id
    }
  };
}