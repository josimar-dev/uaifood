import React, { Component } from 'react';
import Container from '../components/Container';
import Text from '../components/Text';
import { searchRestaurants } from '../connections';
import InputAutoComplete from '../components/InputAutoComplete';
import Button from '../components/Button';
import Image from '../components/Image';
import { connect } from 'react-redux';
import { updateSelectedCity, callRestaurantsResults, updateCityId } from '../actions/actions';

class Home extends Component {

  constructor(props) {
    super(props);
    this.state = { city: null, cityId: null };
  }

  selectCity = (city, cityId) => {
    this.setState({ city, cityId });
  }

  sendSelectedCity = () => {
    let searchParams = {
      city_id: this.state.cityId,
      cuisines: '',
      sort: ''
    };
    searchRestaurants(searchParams)
      .then( data => {
        this.props.callRestaurantsResults(data);
      });
    this.props.updateSelectedCity(this.state.city);
    this.props.updateCityId(this.state.cityId);
    this.props.history.push('/restaurants');
  }

  render() {
    return (
      <Container
        flexDirection='column'
      >
        <Image
          source='/images/logo-white.jpg'
          name='uaifood-logo'
          background='transparent'
          zIndex='999'
          position='absolute'
          width='100%'
          justifyContent='center'
          display='flex'
          padding='36px'
        />
        <Container
          background='url("/images/bg.jpg")'
          height='100vh'
          justifyContent='center'
          alignItems='center'
        >
          <Container
            maxWidth='70%'
            flexDirection='column'
          >
            <Text
              text='Descubra os melhores restaurantes em sua cidade'
              fontSize='60px'
              color='#fff'
              fontWeight={800}
              padding='0px 0px 24px 0px'
            />
            <Container
              flexWrap='noWrap'
              justifyContent='space-between'
              position='relative'
              height='50px'
            >
              <Image
                width='14px'
                position='absolute'
                left='16px'
                zIndex='999'
                top='16px'
                source='/images/map-pin.svg'
                display='inline-block'
              />
              <InputAutoComplete pathname={this.props.history} getCity={this.selectCity} />
              <Button text='BUSCAR' onClick={this.sendSelectedCity}/>
            </Container>
          </Container>
        </Container>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  state
});

const mapActionsToProps = {
  updateSelectedCity: updateSelectedCity,
  callRestaurantsResults: callRestaurantsResults,
  updateCityId: updateCityId
};

export default connect(mapStateToProps, mapActionsToProps)(Home);