import React, { Component } from 'react';
import Header from '../components/Header';
import Container from '../components/Container';
import Text from '../components/Text';
import { connect } from 'react-redux';
import {  getLocation } from '../connections';
import Card from '../components/Card';
import Checkbox from '../components/Checkbox';
import Image from '../components/Image';
import Loading from 'react-fullscreen-loading';
import {updateFoods, updateCost, updateRating} from '../actions/actions';

class Restaurants extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  getCheckboxValues = (e) => {
    let foods = {
      arabic: this.props.foods.arabic,
      brazilian: this.props.foods.brazilian,
      chinese: this.props.foods.chinese,
      french: this.props.foods.french,
      seafood: this.props.foods.seafood,
      italian: this.props.foods.italian,
      japanese: this.props.foods.japanese,
      mexican: this.props.foods.mexican,
      peru: this.props.foods.peru
    };
    switch(e.target.name) {
      case 'arabic':
        foods.arabic = !this.props.foods.arabic;
        foods.brazilian = false;
        foods.chinese = false;
        foods.french = false;
        foods.seafood = false;
        foods.italian = false;
        foods.japanese = false;
        foods.mexican = false;
        foods.peru = false;
        this.props.updateFoods(foods);
      return;
      case 'brazilian':
        foods.brazilian = !this.props.foods.brazilian;
        foods.arabic = false;
        foods.chinese = false;
        foods.french = false;
        foods.seafood = false;
        foods.italian = false;
        foods.japanese = false;
        foods.mexican = false;
        foods.peru = false;
        this.props.updateFoods(foods);
      return;
      case 'chinese':
        foods.chinese = !this.props.foods.chinese;
        foods.arabic = false;
        foods.brazilian = false;
        foods.french = false;
        foods.seafood = false;
        foods.italian = false;
        foods.japanese = false;
        foods.mexican = false;
        foods.peru = false;
        this.props.updateFoods(foods);
        return;
      case 'french':
        foods.french = !this.props.foods.french;
        foods.brazilian = false;
        foods.arabic = false;
        foods.chinese = false;
        foods.seafood = false;
        foods.italian = false;
        foods.japanese = false;
        foods.mexican = false;
        foods.peru = false;
        this.props.updateFoods(foods);
        return;
      case 'seafood':
        foods.seafood = !this.props.foods.seafood;
        foods.brazilian = false;
        foods.arabic = false;
        foods.chinese = false;
        foods.french = false;
        foods.italian = false;
        foods.japanese = false;
        foods.mexican = false;
        foods.peru = false;
        this.props.updateFoods(foods);
        return;
      case 'italian':
        foods.italian = !this.props.foods.italian;
        foods.brazilian = false;
        foods.arabic = false;
        foods.chinese = false;
        foods.french = false;
        foods.seafood = false;
        foods.japanese = false;
        foods.mexican = false;
        foods.peru = false;
        this.props.updateFoods(foods);
        return;
      case 'japanese':
        foods.japanese = !this.props.foods.japanese;
        foods.brazilian = false;
        foods.arabic = false;
        foods.chinese = false;
        foods.french = false;
        foods.seafood = false;
        foods.italian = false;
        foods.mexican = false;
        foods.peru = false;
        this.props.updateFoods(foods);
        return;
      case 'mexican':
        foods.mexican = !this.props.foods.mexican;
        foods.brazilian = false;
        foods.arabic = false;
        foods.chinese = false;
        foods.french = false;
        foods.seafood = false;
        foods.italian = false;
        foods.japanese = false;
        foods.peru = false;
        this.props.updateFoods(foods);
        return;
      case 'peru':
        foods.peru = !this.props.foods.peru;
        foods.brazilian = false;
        foods.arabic = false;
        foods.chinese = false;
        foods.french = false;
        foods.seafood = false;
        foods.italian = false;
        foods.japanese = false;
        foods.mexican = false;
        this.props.updateFoods(foods);
        return;
      default:
        return;
    }
  }

  getCosts = (e) => {
    let cost = {
      until_50: this.props.cost.until_50,
      from_50_to_80: this.props.cost.from_50_to_80,
      from_80_to_110: this.props.cost.from_80_to_110,
      above_110: this.props.cost.above_110
    }
    switch(e.target.name) {
      case 'until-50':
        cost.until_50 = !this.props.cost.until_50;
        cost.from_50_to_80 = false;
        cost.from_80_to_110 = false;
        cost.above_110 = false;
        this.props.updateCost(cost);
        return;
      case 'from-50-to-80':
        cost.until_50 = false;
        cost.from_50_to_80 = !this.props.cost.from_50_to_80;
        cost.from_80_to_110 = false;
        cost.above_110 = false;
        this.props.updateCost(cost);
        return;
      case 'from-80-to-110':
        cost.until_50 = false;
        cost.from_50_to_80 = false;
        cost.from_80_to_110 = !this.props.cost.from_80_to_110;
        cost.above_110 = false;
        this.props.updateCost(cost);
        return;
      case 'above-110':
        cost.until_50 = false;
        cost.from_50_to_80 = false;
        cost.from_80_to_110 = false;
        cost.above_110 = !this.props.cost.above_110;
        this.props.updateCost(cost);
        return;
      default:
        return;
    }
  }

  getRatings = (e) => {
    let ratings = {
      oneStar: this.props.rating.oneStar,
      twoStars: this.props.rating.twoStar,
      threeStars: this.props.rating.threeStars,
      fourStars: this.props.rating.fourStars,
      fiveStars: this.props.rating.fiveStars
    };
    switch(e.target.name) {
      case 'oneStar':
      ratings.oneStar = !this.props.rating.oneStar;
      ratings.twoStars = false;
      ratings.threeStars = false;
      ratings.fourStars = false;
      ratings.fiveStars = false;
      this.props.updateRating(ratings);
      return;
      case 'twoStars':
      ratings.oneStar = false;
      ratings.twoStars = !this.props.rating.twoStars;
      ratings.threeStars = false;
      ratings.fourStars = false;
      ratings.fiveStars = false;
      this.props.updateRating(ratings);
      return;
      case 'threeStars':
      ratings.oneStar = false;
      ratings.twoStars = false;
      ratings.threeStars = !this.props.rating.threeStars;
      ratings.fourStars = false;
      ratings.fiveStars = false;
      this.props.updateRating(ratings);
      return;
      case 'fourStars':
      ratings.oneStar = false;
      ratings.twoStars = false;
      ratings.threeStars = false;
      ratings.fourStars = !this.props.rating.fourStars;
      ratings.fiveStars = false;
      this.props.updateRating(ratings);
      return;
      case 'fiveStars':
      ratings.oneStar = false;
      ratings.twoStars = false;
      ratings.threeStars = false;
      ratings.fourStars = false;
      ratings.fiveStars = !this.props.rating.fiveStars;
      this.props.updateRating(ratings);
      return;
      default:
        return;
    }
  }

  renderSearchResults = () => {
    if (this.props && this.props.restaurantResults && this.props.restaurantResults.restaurants) {
      let results = [];

      // ============================================ FOOD FILTERING ================================ //

      // SEAFOOD
      if (this.props.foods && this.props.foods.seafood) {

        const seafoodFiltering = this.props.restaurantResults.restaurants.filter(type => type.restaurant.cuisines === 'Seafood');
        seafoodFiltering.map(result => {
          results.push(
            <Card
              key={result.restaurant.id}
              restaurantName={result.restaurant.name}
              restaurantAddress={result.restaurant.location.address}
              restaurantCurrency={result.restaurant.currency}
              restaurantCost={result.restaurant.average_cost_for_two}
              restaurantCuisines='Frutos do mar'
              restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
            />
          )
        })
        return results;
      }

      // FRENCH
      if (this.props.foods && this.props.foods.french) {

        const seafoodFiltering = this.props.restaurantResults.restaurants.filter(type => type.restaurant.cuisines === 'French');
        seafoodFiltering.map(result => {

          results.push(
            <Card
              key={result.restaurant.id}
              restaurantName={result.restaurant.name}
              restaurantAddress={result.restaurant.location.address}
              restaurantCurrency={result.restaurant.currency}
              restaurantCost={result.restaurant.average_cost_for_two}
              restaurantCuisines='Francesa'
              restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
            />
          )
        })
        return results;
      }

      // JAPANESE
      if (this.props.foods && this.props.foods.japanese) {

        const seafoodFiltering = this.props.restaurantResults.restaurants.filter(type => type.restaurant.cuisines === 'Japanese');
        seafoodFiltering.map(result => {

          results.push(
            <Card
              key={result.restaurant.id}
              restaurantName={result.restaurant.name}
              restaurantAddress={result.restaurant.location.address}
              restaurantCurrency={result.restaurant.currency}
              restaurantCost={result.restaurant.average_cost_for_two}
              restaurantCuisines='Japonesa'
              restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
            />
          )
        })
        return results;
      }

      //ITALIAN
      if (this.props.foods && this.props.foods.italian) {

        const seafoodFiltering = this.props.restaurantResults.restaurants.filter(type => type.restaurant.cuisines === 'Italian');
        seafoodFiltering.map(result => {

          results.push(
            <Card
              key={result.restaurant.id}
              restaurantName={result.restaurant.name}
              restaurantAddress={result.restaurant.location.address}
              restaurantCurrency={result.restaurant.currency}
              restaurantCost={result.restaurant.average_cost_for_two}
              restaurantCuisines='Italiana'
              restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
            />
          )
        })
        return results;
      }

      //BRAZILIAN
      if (this.props.foods && this.props.foods.brazilian) {

        const seafoodFiltering = this.props.restaurantResults.restaurants.filter(type => type.restaurant.cuisines === 'Brazilian');
        seafoodFiltering.map(result => {

          results.push(
            <Card
              key={result.restaurant.id}
              restaurantName={result.restaurant.name}
              restaurantAddress={result.restaurant.location.address}
              restaurantCurrency={result.restaurant.currency}
              restaurantCost={result.restaurant.average_cost_for_two}
              restaurantCuisines='Brasileira'
              restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
            />
          )
        })
        return results;
      }

      //CHINESE
      if (this.props.foods && this.props.foods.chinese) {

        const seafoodFiltering = this.props.restaurantResults.restaurants.filter(type => type.restaurant.cuisines === 'Chinese');
        seafoodFiltering.map(result => {

          results.push(
            <Card
              key={result.restaurant.id}
              restaurantName={result.restaurant.name}
              restaurantAddress={result.restaurant.location.address}
              restaurantCurrency={result.restaurant.currency}
              restaurantCost={result.restaurant.average_cost_for_two}
              restaurantCuisines='Chinesa'
              restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
            />
          )
        })
        return results;
      }

      //ARABIAN
      if (this.props.foods && this.props.foods.arabic) {

        const seafoodFiltering = this.props.restaurantResults.restaurants.filter(type => type.restaurant.cuisines === 'Arabian');
        seafoodFiltering.map(result => {

          results.push(
            <Card
              key={result.restaurant.id}
              restaurantName={result.restaurant.name}
              restaurantAddress={result.restaurant.location.address}
              restaurantCurrency={result.restaurant.currency}
              restaurantCost={result.restaurant.average_cost_for_two}
              restaurantCuisines='Árabe'
              restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
            />
          )
        })
        return results;
      }

      //MEXICAN
      if (this.props.foods && this.props.foods.mexican) {

        const seafoodFiltering = this.props.restaurantResults.restaurants.filter(type => type.restaurant.cuisines === 'Mexican');
        seafoodFiltering.map(result => {

          results.push(
            <Card
              key={result.restaurant.id}
              restaurantName={result.restaurant.name}
              restaurantAddress={result.restaurant.location.address}
              restaurantCurrency={result.restaurant.currency}
              restaurantCost={result.restaurant.average_cost_for_two}
              restaurantCuisines='Mexicana'
              restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
            />
          )
        })
        return results;
      }

      //PERUVIAN
      if (this.props.foods && this.props.foods.peru) {

        const seafoodFiltering = this.props.restaurantResults.restaurants.filter(type => type.restaurant.cuisines === 'Peruvian');
        seafoodFiltering.map(result => {

          results.push(
            <Card
              key={result.restaurant.id}
              restaurantName={result.restaurant.name}
              restaurantAddress={result.restaurant.location.address}
              restaurantCurrency={result.restaurant.currency}
              restaurantCost={result.restaurant.average_cost_for_two}
              restaurantCuisines='Peruana'
              restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
            />
          )
        })
        return results;
      }


 // =====================================   FOOD FILTERING END ============================================ //



 // =================================== COST FILTERING ====================================================== //

 // UNTIL 50
 if (this.props.cost && this.props.cost.until_50) {

  const seafoodFiltering = this.props.restaurantResults.restaurants.filter(type => Number(type.restaurant.average_cost_for_two) <= 50);
  seafoodFiltering.map(result => {

    results.push(
      <Card
        key={result.restaurant.id}
        restaurantName={result.restaurant.name}
        restaurantAddress={result.restaurant.location.address}
        restaurantCurrency={result.restaurant.currency}
        restaurantCost={result.restaurant.average_cost_for_two}
        restaurantCuisines={result.restaurant.cuisines}
        restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
      />
    )
  })
  return results;
}

// FROM 50 TO 80
if (this.props.cost && this.props.cost.from_50_to_80) {

  const seafoodFiltering = this.props.restaurantResults.restaurants.filter(
    type => (Number(type.restaurant.average_cost_for_two) > 50 && Number(type.restaurant.average_cost_for_two) <= 80)
  );
  seafoodFiltering.map(result => {

    results.push(
      <Card
        key={result.restaurant.id}
        restaurantName={result.restaurant.name}
        restaurantAddress={result.restaurant.location.address}
        restaurantCurrency={result.restaurant.currency}
        restaurantCost={result.restaurant.average_cost_for_two}
        restaurantCuisines={result.restaurant.cuisines}
        restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
      />
    )
  })
  return results;
}

// FROM 80 TO 110
if (this.props.cost && this.props.cost.from_80_to_110) {

  const seafoodFiltering = this.props.restaurantResults.restaurants.filter(
    type => (Number(type.restaurant.average_cost_for_two) > 80 && Number(type.restaurant.average_cost_for_two) <= 110)
  );
  seafoodFiltering.map(result => {

    results.push(
      <Card
        key={result.restaurant.id}
        restaurantName={result.restaurant.name}
        restaurantAddress={result.restaurant.location.address}
        restaurantCurrency={result.restaurant.currency}
        restaurantCost={result.restaurant.average_cost_for_two}
        restaurantCuisines={result.restaurant.cuisines}
        restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
      />
    )
  })
  return results;
}

// ABOVE 110
if (this.props.cost && this.props.cost.above_110) {

  const seafoodFiltering = this.props.restaurantResults.restaurants.filter(
    type => Number(type.restaurant.average_cost_for_two) > 110
  );
  seafoodFiltering.map(result => {

    results.push(
      <Card
        key={result.restaurant.id}
        restaurantName={result.restaurant.name}
        restaurantAddress={result.restaurant.location.address}
        restaurantCurrency={result.restaurant.currency}
        restaurantCost={result.restaurant.average_cost_for_two}
        restaurantCuisines={result.restaurant.cuisines}
        restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
      />
    )
  })
  return results;
}

// ============================================== COST FILTERING END ======================================


// ================================== RATING FILTER ==============================================

// 5 STARS
if (this.props.cost && this.props.rating.fiveStars) {

  const seafoodFiltering = this.props.restaurantResults.restaurants.filter(
    type => Number(type.restaurant.user_rating.aggregate_rating) >= 4.5
  );
  seafoodFiltering.map(result => {

    results.push(
      <Card
        key={result.restaurant.id}
        restaurantName={result.restaurant.name}
        restaurantAddress={result.restaurant.location.address}
        restaurantCurrency={result.restaurant.currency}
        restaurantCost={result.restaurant.average_cost_for_two}
        restaurantCuisines={result.restaurant.cuisines}
        restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
      />
    )
  })
  return results;
}

// 4 STARS
if (this.props.cost && this.props.rating.fourStars) {

  const seafoodFiltering = this.props.restaurantResults.restaurants.filter(
    type => (Number(type.restaurant.user_rating.aggregate_rating) < 4.5 && Number(type.restaurant.user_rating.aggregate_rating) >= 3.5)
  );
  seafoodFiltering.map(result => {

    results.push(
      <Card
        key={result.restaurant.id}
        restaurantName={result.restaurant.name}
        restaurantAddress={result.restaurant.location.address}
        restaurantCurrency={result.restaurant.currency}
        restaurantCost={result.restaurant.average_cost_for_two}
        restaurantCuisines={result.restaurant.cuisines}
        restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
      />
    )
  })
  return results;
}

// 3 STARS
if (this.props.cost && this.props.rating.fourStars) {

  const seafoodFiltering = this.props.restaurantResults.restaurants.filter(
    type => (Number(type.restaurant.user_rating.aggregate_rating) < 3.5 && Number(type.restaurant.user_rating.aggregate_rating) >= 2.5)
  );
  seafoodFiltering.map(result => {

    results.push(
      <Card
        key={result.restaurant.id}
        restaurantName={result.restaurant.name}
        restaurantAddress={result.restaurant.location.address}
        restaurantCurrency={result.restaurant.currency}
        restaurantCost={result.restaurant.average_cost_for_two}
        restaurantCuisines={result.restaurant.cuisines}
        restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
      />
    )
  })
  return results;
}

// 2 STARS
if (this.props.cost && this.props.rating.fourStars) {

  const seafoodFiltering = this.props.restaurantResults.restaurants.filter(
    type => (Number(type.restaurant.user_rating.aggregate_rating) < 2.5 && Number(type.restaurant.user_rating.aggregate_rating) >= 1.5)
  );
  seafoodFiltering.map(result => {

    results.push(
      <Card
        key={result.restaurant.id}
        restaurantName={result.restaurant.name}
        restaurantAddress={result.restaurant.location.address}
        restaurantCurrency={result.restaurant.currency}
        restaurantCost={result.restaurant.average_cost_for_two}
        restaurantCuisines={result.restaurant.cuisines}
        restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
      />
    )
  })
  return results;
}

// 1 STAR
if (this.props.cost && this.props.rating.fourStars) {

  const seafoodFiltering = this.props.restaurantResults.restaurants.filter(
    type => (Number(type.restaurant.user_rating.aggregate_rating) < 1.5 && Number(type.restaurant.user_rating.aggregate_rating) > 0)
  );
  seafoodFiltering.map(result => {

    results.push(
      <Card
        key={result.restaurant.id}
        restaurantName={result.restaurant.name}
        restaurantAddress={result.restaurant.location.address}
        restaurantCurrency={result.restaurant.currency}
        restaurantCost={result.restaurant.average_cost_for_two}
        restaurantCuisines={result.restaurant.cuisines}
        restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
      />
    )
  })
  return results;
}


// ============================================= RATING FILTER END ==========================================



      this.props.restaurantResults.restaurants.map(result => {

        results.push(
          <Card
            key={result.restaurant.id}
            restaurantName={result.restaurant.name}
            restaurantAddress={result.restaurant.location.address}
            restaurantCurrency={result.restaurant.currency}
            restaurantCost={result.restaurant.average_cost_for_two}
            restaurantCuisines={result.restaurant.cuisines}
            restaurantRating={Number(result.restaurant.user_rating.aggregate_rating)}
          />
        );
      })

      return results;
    }
    return <Loading loading={true} background='#fff' loaderColor="#3498db"/>;
  }

  render() {
    return(
      <Container
        background='#f4f4f4'
        flexDirection='column'
        flexWrap='noWrap'

      >
        <Header pathname={this.props.history} />
        <Container
          flexWrap='noWrap'
          flexDirection='row'
        >
          <Container
          boxShadow={`0px 1px 3px 0px rgba(0, 0, 0, 0.2),
          0px 1px 1px 0px rgba(0, 0, 0, 0.14),
          0px 2px 1px -1px rgba(0, 0, 0, 0.12)`}
          width='15%'
          margin='16px'
          flexWrap='wrap'
          background='#fff'
          display='inline-table'
          >

            <Container flexDirection='column' flexWrap='wrap' margin='16px'>
              <Text text='NOTA' color='#a5a5a5' padding='0px 0px 12px 0px' fontSize='14px' fontWeight={300} />
              <Checkbox isChecked={this.props.rating.oneStar} name='oneStar' toggleChange={this.getRatings} marginLabel='0px 0px 0px 10px' label={<Image source='/images/star-regular.svg' width='18px' />}/>
              <Checkbox isChecked={this.props.rating.twoStars} name='twoStars' toggleChange={this.getRatings} marginLabel='0px 0px 0px 10px' label={[
                <Image source='/images/star-regular.svg' width='18px' />,
                <Image source='/images/star-regular.svg' width='18px' />
              ]}/>
              <Checkbox isChecked={this.props.rating.threeStars} name='threeStars' toggleChange={this.getRatings} marginLabel='0px 0px 0px 10px' label={[
                <Image  source='/images/star-regular.svg' width='18px' />,
                <Image source='/images/star-regular.svg' width='18px' />,
                <Image source='/images/star-regular.svg' width='18px' />
              ]}/>
              <Checkbox isChecked={this.props.rating.fourStars} name='fourStars' toggleChange={this.getRatings} marginLabel='0px 0px 0px 10px' label={[
                <Image source='/images/star-regular.svg' width='18px' />,
                <Image source='/images/star-regular.svg' width='18px' />,
                <Image source='/images/star-regular.svg' width='18px' />,
                <Image source='/images/star-regular.svg' width='18px' />,
              ]}/>
              <Checkbox isChecked={this.props.rating.fiveStars} name='fiveStars' toggleChange={this.getRatings} marginLabel='0px 0px 0px 10px' label={[
                <Image source='/images/star-regular.svg' width='18px' />,
                <Image source='/images/star-regular.svg' width='18px' />,
                <Image source='/images/star-regular.svg' width='18px' />,
                <Image source='/images/star-regular.svg' width='18px' />,
                <Image source='/images/star-regular.svg' width='18px' />
              ]}/>
            </Container>

            <Container flexDirection='column' flexWrap='wrap' margin='16px'>
              <Text text='CUSTO PARA 2 PESSOAS' color='#a5a5a5' padding='0px 0px 12px 0px' fontSize='14px' fontWeight={300} />
              <Checkbox name='until-50' toggleChange={this.getCosts} isChecked={this.props.cost.until_50} label={<Text  padding='0px 0px 0px 10px' text='Até R$50' fontSize='14px' fontWeight={300} color='#484848' />}/>
              <Checkbox name='from-50-to-80' toggleChange={this.getCosts} isChecked={this.props.cost.from_50_to_80} label={<Text  padding='0px 0px 0px 10px' text='De R$50 a R$80' fontSize='14px' fontWeight={300} color='#484848' />}/>
              <Checkbox name='from-80-to-110' toggleChange={this.getCosts} isChecked={this.props.cost.from_80_to_110} label={<Text  padding='0px 0px 0px 10px' text='De R$80 a R$110' fontSize='14px' fontWeight={300} color='#484848' />}/>
              <Checkbox name='above-110' toggleChange={this.getCosts} isChecked={this.props.cost.above_110} label={<Text  padding='0px 0px 0px 10px' text='Acima de R$110' fontSize='14px' fontWeight={300} color='#484848' />}/>
            </Container>

            <Container flexDirection='column' flexWrap='wrap' margin='16px'>
              <Text text='TIPO DE COZINHA' color='#a5a5a5' padding='0px 0px 12px 0px' fontSize='14px' fontWeight={300} />
              <Checkbox isChecked={this.props.foods.arabic} name='arabic' toggleChange={this.getCheckboxValues} label={<Text  padding='0px 0px 0px 10px' text='Árabe' fontSize='14px' fontWeight={300} color='#484848' />}/>
              <Checkbox isChecked={this.props.foods.brazilian} name='brazilian' toggleChange={this.getCheckboxValues} label={<Text  padding='0px 0px 0px 10px' text='Brasileira' fontSize='14px' fontWeight={300} color='#484848' />}/>
              <Checkbox isChecked={this.props.foods.chinese} name='chinese' toggleChange={this.getCheckboxValues} label={<Text  padding='0px 0px 0px 10px' text='Chinesa' fontSize='14px' fontWeight={300} color='#484848' />}/>
              <Checkbox isChecked={this.props.foods.french} name='french' toggleChange={this.getCheckboxValues} label={<Text  padding='0px 0px 0px 10px' text='Francesa' fontSize='14px' fontWeight={300} color='#484848' />}/>
              <Checkbox isChecked={this.props.foods.seafood} name='seafood' toggleChange={this.getCheckboxValues} label={<Text  padding='0px 0px 0px 10px' text='Frutos do mar' fontSize='14px' fontWeight={300} color='#484848' />}/>
              <Checkbox isChecked={this.props.foods.italian} name='italian' toggleChange={this.getCheckboxValues} label={<Text  padding='0px 0px 0px 10px' text='Italiana' fontSize='14px' fontWeight={300} color='#484848' />}/>
              <Checkbox isChecked={this.props.foods.japanese} name='japanese' toggleChange={this.getCheckboxValues} label={<Text  padding='0px 0px 0px 10px' text='Japonesa' fontSize='14px' fontWeight={300} color='#484848' />}/>
              <Checkbox isChecked={this.props.foods.mexican} name='mexican' toggleChange={this.getCheckboxValues} label={<Text  padding='0px 0px 0px 10px' text='Mexicana' fontSize='14px' fontWeight={300} color='#484848' />}/>
              <Checkbox isChecked={this.props.foods.peru} name='peru' toggleChange={this.getCheckboxValues} label={<Text  padding='0px 0px 0px 10px' text='Peruana' fontSize='14px' fontWeight={300} color='#484848' />}/>
            </Container>

          </Container>
          <Container
            flexDirection='column'
            flexWrap='wrap'
            width='85%'

          >
            <Container
              padding='16px 0px'
            >
              <Text
                text={`Restaurantes em ${this.props.selectedCity}`}
                fontWeight={600}
                fontSize='24px'
              />
            </Container>


            <Container
              flexWrap='wrap'
              justifyContent='flex-start'
            >
              {this.renderSearchResults()}
            </Container>
          </Container>


        </Container>


      </Container>
    );
  }
}

const mapStateToProps = state => ({
  restaurantResults: state.restaurantResults,
  selectedCity: state.selectedCity,
  foods: state.foods,
  cost: state.cost,
  rating: state.rating
});

const mapActionsToProps = {
  updateFoods: updateFoods,
  updateCost: updateCost,
  updateRating: updateRating
};

export default connect(mapStateToProps, mapActionsToProps)(Restaurants);