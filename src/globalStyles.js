import { injectGlobal } from 'styled-components';
import { normalize } from 'polished';

const globalStyles = () =>
  injectGlobal`
    ${normalize()}
    * {
      box-sizing: border-box;
    }

    body {
      font-family: 'Open Sans', sans-serif;
    }
  `;

export default globalStyles;