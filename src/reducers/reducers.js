import { UPDATE_SELECTED_CITY, CALL_RESTAURANTS_RESULTS, UPDATE_CITY_ID, UPDATE_FOODS, UPDATE_COST, UPDATE_RATING } from '../actions/actions';

const initialFoods = {
  arabic: false,
  brazilian: false,
  chinese: false,
  french: false,
  seafood: false,
  italian: false,
  japanese: false,
  mexican: false,
  peru: false
};

const initialCost = {
  until_50: false,
  from_50_to_80: false,
  from_80_to_110: false,
  above_110: false
}

const initialRating = {
  oneStar: false,
  twoStars: false,
  threeStars: false,
  fourStars: false,
  fiveStars: false
};

export function rating (state = initialRating, { type, payload }) {
  switch(type) {
    case UPDATE_RATING:
    return payload.rating
    default:
      return state;
  }
}

export function cost (state = initialCost, { type, payload }) {
  switch(type) {
    case UPDATE_COST:
    return payload.cost
    default:
      return state;
  }
}


export function foods (state = initialFoods, { type, payload }) {
  switch(type) {
    case UPDATE_FOODS:
    return payload.foods
    default:
      return state;
  }
}

export function selectedCity (state = {}, { type, payload }) {
  switch (type) {
    case UPDATE_SELECTED_CITY:
      return payload.selectedCity;
      default:
        return state;
  }
}

export function restaurantResults (state = {}, { type, payload }) {
  switch(type) {
    case CALL_RESTAURANTS_RESULTS:
      return payload.restaurantResults;
      default:
        return state;
  }
}

export function cityId (state = {}, { type, payload }) {
  switch(type) {
    case UPDATE_CITY_ID:
      return payload.cityId;
      default:
        return state;
  }
}