const initialState = {
  selectedCity: null,
  cityId: null,
  restaurantResults: null,
  foods: null,
  cost: null,
  rating: null
};