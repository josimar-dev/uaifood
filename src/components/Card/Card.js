import React, {Fragment, Component} from 'react';
import Text from '../Text';
import Container from '../Container';
import Image from '../Image';

class Card extends Component {


  renderRatings = () => {
    let ratings = [];
    if (this.props && this.props.restaurantRating > 4.5) {
      ratings.push(
        <Fragment key={0}>
          <Image width='16px' source='/images/star-solid.svg' />
          <Image width='16px' source='/images/star-solid.svg' />
          <Image width='16px' source='/images/star-solid.svg' />
          <Image width='16px' source='/images/star-solid.svg' />
          <Image width='16px' source='/images/star-solid.svg' />
        </Fragment>
      )

      return ratings;
    } else if (this.props && (this.props.restaurantRating  < 4.5 || this.props.restaurantRating > 3.5)) {
      ratings.push(
        <Fragment  key={1}>
          <Image width='16px' source='/images/star-solid.svg' />
          <Image width='16px' source='/images/star-solid.svg' />
          <Image width='16px' source='/images/star-solid.svg' />
          <Image width='16px' source='/images/star-solid.svg' />
        </Fragment>
      )
      return ratings;
    } else if (this.props && (this.props.restaurantRating  < 3.5 || this.props.restaurantRating > 2.5)) {
      ratings.push(
        <Fragment key={2}>
          <Image width='16px' source='/images/star-solid.svg' />
          <Image width='16px' source='/images/star-solid.svg' />
          <Image width='16px' source='/images/star-solid.svg' />
        </Fragment>
      );
      return ratings;
    } else if (this.props && (this.props.restaurantRating  < 2.5 || this.props.restaurantRating > 1.5)) {
      ratings.push(
        <Fragment key={3}>
          <Image width='16px' source='/images/star-solid.svg' />
          <Image width='16px' source='/images/star-solid.svg' />
        </Fragment>
      )
      return ratings;
    } else if (this.props && (this.props.restaurantRating  < 1.5 || this.props.restaurantRating > 0.5)) {
      ratings.push(<Fragment key={4}><Image width='16px' source='/images/star-solid.svg' /></Fragment>);
      return ratings;
    }
    return ratings;
  }


  render () {
    return(
      <Container
     flexDirection='column'
     flexWrap='wrap'
     width='32%'
     boxShadow={`0px 1px 3px 0px rgba(0, 0, 0, 0.2),
     0px 1px 1px 0px rgba(0, 0, 0, 0.14),
     0px 2px 1px -1px rgba(0, 0, 0, 0.12)`}
     margin='0px 14px 14px 0px'
     background='#fff'
     justifyContent='space-between'
   >
     <Container display='inline-block'>
       <Image source='/images/spice-nice-restaurant.jpg' />
     </Container>
     <Container
       background='#fff'
       flexDirection='column'
       padding='12px'>
       <Text
         text={this.props.restaurantName}
         fontWeight={600}
         color='#333'
         fontSize='20px'
         padding='0px 0px 10px 0px'
       />
       <Text
         text={this.props.restaurantAddress}
         fontWeight={300}
         fontSize='14px'
         color='#484848'
       />
     </Container>
     <Container background='#fff' padding='0px 10px 10px'>
       {this.renderRatings()}
     </Container>
     <Container background='#fff' padding='0px 10px 10px'>
       <Container margin='0px 16px 0px 0px' padding='6px 14px' width='auto' background='#39b54a'>
         <Image width='16px' source='/images/user-friends-solid.svg' />
         <Text fontSize='14px' text={`${this.props.restaurantCurrency} ${this.props.restaurantCost}`}  fontWeight={600} color='#fff' padding='0px 0px 0px 6px' />
       </Container>
       <Container
         padding='6px 14px'
         width='auto'
         background='#ccc'
       >
         <Text fontSize='14px' color='#484848' fontWeight={300} text={this.props.restaurantCuisines} />
       </Container>
     </Container>
   </Container>
    );
  }
}


export default Card;