import React from 'react';
import Styled from 'styled-components';

const ContainerStyles = Styled.div`
  display: ${props => props.display};
  flex-direction: ${props => props.flexDirection};
  flex-wrap: ${props => props.flexWrap};
  justify-content: ${props => props.justifyContent};
  align-items: ${props => props.alignItems};
  background: ${props => props.background};
  margin: ${props => props.margin};
  padding: ${props => props.padding};
  width: ${props => props.width};
  height: ${props => props.height};
  background-size: ${props => props.backgroundSize};
  max-width: ${props => props.maxWidth};
  position: ${props => props.position};
  box-shadow: ${props => props.boxShadow};
  border: ${props => props.border};
`;

const Container = props => (
  <ContainerStyles {...props}>{props.children}</ContainerStyles>
);

Container.defaultProps = {
  width: '100%',
  height: 'auto',
  margin: 0,
  padding: 0,
  backgroundSize: 'cover',
  maxWidth: '100%',
  display: 'flex'
}

export default Container;