import React from 'react';
import Container from '../Container';

const Checkbox = props => (
  <Container
    height='32px'
    alignItems='center'
  >
    <input name={props.name} type='checkbox' checked={props.isChecked} onChange={props.toggleChange} />
    <Container margin={props.marginLabel}>{props.label}</Container>
  </Container>
);

export default Checkbox;