import React from 'react';
import PropTypes from 'prop-types';
import Styled from 'styled-components';

const ImageStyles = Styled.div`
    max-width: ${props => props.maxW};
    width: ${props => props.width};
    height: ${props => props.height};
    margin: ${props => props.marg};
    background: ${props => props.background};
    z-index: ${props => props.zIndex};
    display: ${props => props.display};
    justify-content: ${props => props.justifyContent};
    position: ${props => props.position};
    padding: ${props => props.padding};
    top: ${props => props.top};
    left: ${props => props.left};
    > img {
        max-width: 100%;
        height: auto;
    }
`;

const Image = props => (
  <ImageStyles {...props}>
    <img src={props.source} alt={props.name}/>
  </ImageStyles>
);

Image.propTypes = {
  source: PropTypes.string,
  alt: PropTypes.string
};

export default Image;