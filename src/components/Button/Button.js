import React from 'react';
import PropTypes from 'prop-types';
import Styled from 'styled-components';
import theme from '../../theme';

const ButtonStyles = Styled.button`
  border: ${props => props.border};
  border-radius: ${props => props.borderRadius};
  color: ${props => props.color};
  background: ${props => props.background};
  padding: ${props => props.padding};
  margin: ${props => props.margin};
  width: ${props => props.width};
  font-weight: ${props => props.fontWeight};
  cursor: pointer;
`;

const Button = props => (
  <ButtonStyles {...props}>{props.text}</ButtonStyles>
);

Button.defaultProps = {
  color:`${theme.color_2}`,
  border: `1px solid ${theme.color_4}`,
  borderRadius: '2px',
  padding: '10px 60px',
  background: `${theme.color_4}`,
  width: 'auto',
  fontWeight: 600
};

Button.propTypes = {
  text: PropTypes.string
};

export default Button;