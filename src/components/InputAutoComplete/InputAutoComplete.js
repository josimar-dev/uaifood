import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';
import styles from './dependency/styles.css';
import { getCitiesDetails } from '../../connections';
import Container from '../Container';
import Styled from 'styled-components';

const CustomContainer = Styled(Container)`
	border: 1px solid #ccc;
	height: 52px;
	border-radius: 3px;
	margin-right: 20px;
	> div.react-autosuggest__container--open {
		color: red;
		>.react-autosuggest__suggestions-container {
			width: 100%!important;
		}
	}
`;


function renderSuggestion(suggestion) {
  return(
    <div className='react-autosuggest__item'>
      <span className='react-autosuggest__title'>{suggestion.name}</span>
      <span className='react-autosuggest__name'>{`Estado - ${suggestion.state_name}`}</span>
    </div>
  );
}

function renderSectionTitle(section) {
  return(
    <strong>{section.parent.name}</strong>
  );
}

function getSectionSuggestions(section) {
  let sectionName = [section];
  return sectionName;
}

class InputAutoComplete extends Component {
  constructor(props) {
    super(props);
    this.state = {
			value: '',
      suggestions: [],
      cityId: null
    }
	}

	componentDidMount = () => {
		if (this.props && this.props.selectedCity) {
			this.setState({ value: this.props.selectedCity })
		}
	}


  onChange = (event, { newValue, method }) => {
		this.setState({
			value: newValue,
		}, () => {
			if (this.props.pathname && this.props.pathname.location.pathname !== '/restaurants') {

				this.props.getCity(this.state.value, this.state.cityId)
				return;
			}
			this.props.getCityHeader(this.state.value, this.state.cityId)
			return;

		});

  };

  onSuggestionsFetchRequested = ({value}) => {
		if(value.length < 3) {
			return;
		}

		getCitiesDetails(value)
			.then(data => this.setState({suggestions: data.location_suggestions}));
  };

  onSuggestionsClearRequested = () => {
		this.setState({
			suggestions: []
		});
	};

	getSuggestionValue = (suggestion) => {
		return `${suggestion.name}`;
	}

  onSuggestionSelected = (event, { suggestion }) => {
		this.setState({
			cityId: suggestion.id
		})
	}

	onBlur = () => {
		let formData = {
			chargeType: this.state.chargeType,
		};
  }

  render () {
		const { value, suggestions } = this.state;

		const inputProps = {
			placeholder: "Digite a sua cidade",
			value,
			onChange: this.onChange,
			onBlur: this.onBlur,
			name: 'cityId'
		};

		if (this.props.pathname && this.props.pathname.location.pathname === '/restaurants') {
			return (
				<CustomContainer>
					<Autosuggest
						suggestions={suggestions}
						onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
						onSuggestionsClearRequested={this.onSuggestionsClearRequested}
						getSuggestionValue={this.getSuggestionValue}
						renderSuggestion={renderSuggestion}
						renderSectionTitle={renderSectionTitle}
						getSectionSuggestions={getSectionSuggestions}
						inputProps={inputProps}
						onSuggestionSelected={this.onSuggestionSelected}
					/>
				</CustomContainer>
			);
		}


		return (
				<Autosuggest
					suggestions={suggestions}
					onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
					onSuggestionsClearRequested={this.onSuggestionsClearRequested}
					getSuggestionValue={this.getSuggestionValue}
					renderSuggestion={renderSuggestion}
					renderSectionTitle={renderSectionTitle}
					getSectionSuggestions={getSectionSuggestions}
					inputProps={inputProps}
					onSuggestionSelected={this.onSuggestionSelected}
				/>
		);
	}

}

export default InputAutoComplete;