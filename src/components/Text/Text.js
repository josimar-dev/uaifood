import React from 'react';
import PropTypes from 'prop-types';
import Styled from 'styled-components';

const TextStyles = Styled.span`
  font-size: ${props => props.fontSize};
  color: ${props => props.color};
  font-family: ${props => props.fontFamily};
  font-weight: ${props => props.fontWeight};
  margin: ${props => props.margin};
  padding: ${props => props.padding};
`;

const Text = props => (
  <TextStyles {...props}>{props.text}</TextStyles>
);

Text.defaultProps = {
  text: 'Hello!',
  margin: 0,
  padding: 0
}

Text.propTypes = {
  text: PropTypes.string
}

export default Text;