import React, { Component } from 'react';
import Container from '../Container';
import Image from '../Image';
import Button from '../Button';
import InputAutoComplete from '../InputAutoComplete';
import {connect} from 'react-redux';
import {searchRestaurants} from '../../connections';
import {callRestaurantsResults, updateSelectedCity, updateCityId} from '../../actions/actions';

class Header extends Component {

  constructor(props) {
    super(props);
    this.state = { city: null, cityId: null };
  }

  selectCityHeader = (city, cityId) => {
    this.setState({ city, cityId })
  }

  // params has to come from redux cuz we're gonna needd them
  // to make the filter work properly...
  sendSelectedCity = () => {
    let cityId = this.state.cityId ? this.state.cityId : this.props.cityId;
    let city = this.state.city ? this.state.city : this.props.selectedCity;
    let params = {
      city_id: cityId,
      cuisines: '',
      sort: ''
    }
    searchRestaurants(params)
      .then(data => {
        this.props.callRestaurantsResults(data)
      });
    this.props.updateSelectedCity(city);
    this.props.updateCityId(cityId);
  }


  render() {
    return(
      <Container
        padding='16px'
        height='80px'
        background='#fff'
        boxShadow={`0px 1px 3px 0px rgba(0, 0, 0, 0.2),
        0px 1px 1px 0px rgba(0, 0, 0, 0.14),
        0px 2px 1px -1px rgba(0, 0, 0, 0.12)`}
        justifyContent='space-between'
        flexWrap='noWrap'
        alignItems='center'
      >
        <Image
          source='/images/logo-red.jpg'
          alt='uaifood-red-logo'
          display='inline-block'
        />
        <Container
          flexDirection='space-between'
          flexWrap='noWrap'
          position='relative'
          height='52px'
          width='80%'
        >
        <Image
          width='14px'
          position='absolute'
          left='16px'
          zIndex='999'
          top='16px'
          source='/images/map-pin.svg'
          display='inline-block'
        />
          <InputAutoComplete
            pathname={this.props.pathname}
            cityId={this.props.cityId}
            getCityHeader={this.selectCityHeader}
            selectedCity={this.props.selectedCity}
          />
          <Button text='BUSCAR' onClick={this.sendSelectedCity} />
        </Container>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  selectedCity: state.selectedCity,
  cityId: state.cityId
});

const mapActionsToProps = {
  callRestaurantsResults: callRestaurantsResults,
  updateSelectedCity: updateSelectedCity,
  updateCityId: updateCityId
};

export default connect(mapStateToProps, mapActionsToProps)(Header);

